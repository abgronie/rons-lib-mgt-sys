from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.home, name='home'),
    path('signup/', views.signup, name='signup'),
    path('home/viewbooks/', views.viewbooks, name='viewbooks'),
    path('home/viewbooks/addbook/', views.addbook, name = 'addbook'),
    path('home/viewbooks/addbook/addrecord/', views.addrecord, name = 'addrecord'),
    path('home/viewbooks/delete/<int:id>', views.delete, name = 'delete'),
    path('home/viewbooks/update/<int:id>', views.update, name='update'),
    path('home/viewbooks/update/updaterecord/<int:id>', views.updaterecord, name='updaterecord'),
]

urlpatterns += staticfiles_urlpatterns()