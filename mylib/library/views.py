from django.shortcuts import render
from django.template import loader
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Book

def index(request):
    template = loader.get_template("index.html")
    return HttpResponse(template.render())

def home(request):
    template = loader.get_template("home.html")
    return HttpResponse(template.render())

def signup(request):
    template = loader.get_template("signup.html")
    return HttpResponse(template.render())

def viewbooks(request):
    mylibrary = Book.objects.all().values()
    template = loader.get_template("viewbooks.html")
    context = {
        "mylibrary": mylibrary,
    }
    return HttpResponse(template.render(context, request))

def addbook(request):
    template = loader.get_template("addbook.html")
    return HttpResponse(template.render({}, request))

def addrecord(request):
    w = request.POST["title"]
    x = request.POST["author"]
    y = request.POST["category"]
    z = request.POST["year"]

    book = Book(title = w, author = x, category = y, year = z)
    book.save()
    return HttpResponseRedirect(reverse("viewbooks"))

def delete(request, id):
    book = Book.objects.get(id=id)
    book.delete()
    return HttpResponseRedirect(reverse("viewbooks"))

def update(request, id):
    mybook = Book.objects.get(id=id)
    template = loader.get_template("update.html")
    context = {
        "mybook": mybook,
    }
    return HttpResponse(template.render(context, request))

def updaterecord(request, id):
    title = request.POST["title"]
    author = request.POST["author"]
    category = request.POST["category"]
    year = request.POST["year"]

    book = Book.objects.get(id=id)
    book.title = title
    book.author = author
    book.category = category
    book.year = year

    book.save()
    return HttpResponseRedirect(reverse("viewbooks"))
